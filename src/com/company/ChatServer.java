package com.company;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;


public class ChatServer extends Thread
{
	private ServerSocket serverSocket;
	private Thread thServer;

	BlockingQueue<SocketProcessor> queue = new LinkedBlockingQueue<SocketProcessor>();



	public ChatServer()
	{
		try
		{
			this.serverSocket = new ServerSocket(5555);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	void StartServer()
	{
		thServer = Thread.currentThread();

		while (true)
		{
			Socket socket = getNewConnect();

			if (thServer.isInterrupted())
			{
				break;
			}
			else if (socket != null)
			{
				try
				{
					final SocketProcessor socketProcessor = new SocketProcessor(socket);

					final Thread thread = new Thread(socketProcessor);

					thread.setDaemon(true);
					thread.start();

					queue.offer(socketProcessor);
				}
				catch (IOException ioe)
				{
					ioe.printStackTrace();
				}
			}
		}
	}

	private synchronized void stopServer()
	{
		for (SocketProcessor sp : queue)
		{
			sp.close();
		}
		if (!serverSocket.isClosed())
		{
			try
			{
				serverSocket.close();
			}
			catch (IOException ignored)
			{
			}
		}
	}

	private Socket getNewConnect()
	{
		Socket tempSocket = null;

		try
		{
			tempSocket = this.serverSocket.accept();
		}
		catch (IOException e)
		{
			stopServer();
			e.printStackTrace();
		}

		return tempSocket;
	}

	@Override
	public void run()
	{
		System.out.println(Thread.currentThread().getName() + ": START server");

		StartServer();
	}


	private class SocketProcessor implements Runnable
	{
		Socket socket;
		BufferedReader br;
		BufferedWriter bw;

		SocketProcessor(Socket socketParam) throws IOException
		{
			socket = socketParam;
			br = new BufferedReader(new InputStreamReader(socket.getInputStream(), "UTF-8"));
			bw = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), "UTF-8"));
		}


		public void run()
		{
			while (!socket.isClosed())
			{
				String line = null;
				try
				{
					line = br.readLine();
				}
				catch (IOException e)
				{
					close();
				}

				if (line == null)
				{
					close();
				}
				else if ("shutdown".equals(line))
				{
					thServer.interrupt();
					try
					{
						new Socket("localhost", 5555);
					}
					catch (IOException ignored)
					{
					}
					finally
					{
						stopServer();
					}
				}
				else
				{
					for (SocketProcessor sp : queue)
					{
						sp.send(line);
					}
				}
			}
		}

		public synchronized void send(String line)
		{
			try
			{
				bw.write(line);
				bw.write("\n");
				bw.flush();
			}
			catch (IOException e)
			{
				close();
			}
		}

		public synchronized void close()
		{
			queue.remove(this);
			if (!socket.isClosed())
			{
				try
				{
					socket.close();
				}
				catch (IOException ignored)
				{
				}
			}
		}

		@Override
		protected void finalize() throws Throwable
		{
			super.finalize();
			close();
		}
	}
}
