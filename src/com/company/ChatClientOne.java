package com.company;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;


public class ChatClientOne
{
	final Socket socket;
	final BufferedReader br;
	final BufferedWriter bwServer;
	final BufferedReader bwInput;


	public ChatClientOne(String host, int port) throws IOException
	{
		socket = new Socket(host, port);

		br = new BufferedReader(new InputStreamReader(socket.getInputStream(), "UTF-8"));
		bwServer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), "UTF-8"));

		bwInput = new BufferedReader(new InputStreamReader(System.in));
		new Thread(new Receiver()).start();
	}

	public void run()
	{
		System.out.println("Type phrase(s) (hit Enter to exit):");
		while (true)
		{
			String userString = null;
			try
			{
				userString = bwInput.readLine();
			}
			catch (IOException ignored)
			{}

			if (userString == null || userString.length() == 0 || socket.isClosed())
			{
				close();
				break;
			}
			else
			{
				try
				{
					bwServer.write(userString);
					bwServer.write("\n");
					bwServer.flush();
				}
				catch (IOException e)
				{
					close();
				}
			}
		}
	}


	public synchronized void close()
	{
		if (!socket.isClosed())
		{
			try
			{
				socket.close();
				System.exit(0);
			}
			catch (IOException ignored)
			{
				ignored.printStackTrace();
			}
		}
	}

	public static void main(String[] args)
	{
		try
		{
			new ChatClientOne("localhost", 5555).run();
		}
		catch (IOException e)
		{
			System.out.println("Unable to connect. Server not running?");
		}
	}


	private class Receiver implements Runnable
	{

		public void run()
		{
			while (!socket.isClosed())
			{
				String line = null;
				try
				{
					line = br.readLine();
				}
				catch (IOException e)
				{
					if ("Socket closed".equals(e.getMessage()))
					{
						break;
					}
					System.out.println("Connection lost");
					close();
				}
				if (line == null)
				{
					System.out.println("Server has closed connection");
					close();
				}
				else
				{
					System.out.println("Server:" + line);
				}
			}
		}
	}
}
